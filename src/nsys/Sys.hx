package nsys;

import js.Node;

class Sys
{
    public static function args():Array<String>
    {
        return Node.process.argv;
    }
}
