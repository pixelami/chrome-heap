package nsys.io;

import js.Node;

class File
{
    public static function getContent(path:String)
    {
        return js.Node.require("fs").readFileSync(path, 'utf8');
    }
}
