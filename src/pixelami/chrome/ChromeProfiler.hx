package pixelami.chrome;

import IntHash;


class ChromeProfiler extends ChromeDebuggerClient
{
    var heapSnapshotChunks:IntHash<Array<String>>;

    // callback to tell us when a snapshot has been taken.
    public var onSnapshot:Heap -> Void;

    public function new()
    {
        super();
        heapSnapshotChunks = new IntHash<Array<String>>();
    }

    override function onConnect()
    {
        trace("connected");
        call( "Debugger.enable");
        call( "Profiler.takeHeapSnapshot" );
    }

    function reportHeapSnapshotProgress(params:HeapSnapshotProgress)
    {
        var percent = (params.done / params.total)  * 100;
        trace("snapshot progress: "+ percent + "%");
    }

    function addProfileHeader(params:ProfileHeaderCreationResponse)
    {
        trace("profile added ... retrieving profile");

        call( "Profiler.getProfileHeaders" );
        //call( "Profiler.clearProfiles" );
        call( "Profiler.getHeapSnapshot", { uid:params.header.uid, type:"HEAP" } );
    }

    function addHeapSnapshotChunk(params:HeapSnapshotChunk)
    {
        if(!heapSnapshotChunks.exists(params.uid)) heapSnapshotChunks.set(params.uid, []);
        heapSnapshotChunks.get(params.uid).push(params.chunk);
    }

    function finishHeapSnapshot(params:{uid:Int})
    {
        var heapSnapshots = heapSnapshotChunks.get(params.uid);
        var snapshot = "";
        while(heapSnapshots.length > 0)
        {
            snapshot += heapSnapshots.shift();
        }
        heapSnapshotChunks.remove(params.uid);
        trace("heap loaded");
        trace("heap parsing ...");
        onSnapshot(new Heap(snapshot));
        trace("heap ready");
    }

    function resetProfiles(?params = null)
    {
        trace("reset profiles");
    }
}

typedef HeapSnapshotProgress = {
    done:Int,
    total:Int
}

typedef HeapSnapshotChunk = {
    uid:Int,
    chunk:String
}

typedef ProfileHeaderCreationResponse = {
    header:ProfileHeader
}

typedef ProfileHeader = {
    typeId:String,
    uid:Int,
    title:String,
    maxJSObjectId:Int
}
