package pixelami.chrome;

import pixelami.net.Token;
import pixelami.nodejs.WebSocket;
import js.Node;

class ChromeDebuggerClient
{
    var socket:WebSocket;
    var WebSocketClass:Class<Dynamic>;
    var messageId:Int;

    var pendingMessage:Bool;
    var requestQueue:Array<Dynamic>;
    var pendingTokens:IntHash<Token>;

    public var requestDelay:Int;

    public function new()
    {
        WebSocketClass = Node.require('ws');
        messageId = 0;
        requestQueue = [];
        pendingTokens = new IntHash<Token>();
        requestDelay = 1000;
    }

    public function connect(url:String)
    {
        socket = Type.createInstance(WebSocketClass,[url]);
        socket.on('open', onConnect);
        socket.on('message', onMessage);
    }

    function onConnect()
    {
        // abstract
    }

    function onMessage(data)
    {
        // Enable this trace to see full responses.
        // The Chrome dev tools profiling API is not stable and subject to change at any moment
        // When using Chrome Canary and something breaks, check this page,
        // https://github.com/WebKit/webkit/blob/master/Source/WebCore/inspector/Inspector.json
        // to see the latest api.

        //trace(data);

        var o:Dynamic = haxe.Json.parse(data);

        if(Reflect.hasField(o,"method"))
        {
            var handler = o.method.split(".");
            var domain = handler[0];
            var method = handler[1];

            try
            {
                Reflect.callMethod(this,Reflect.field(this,method), [o.params]);
            }
            catch(e:Dynamic)
            {
                trace("no handler defined for "+handler);
            }
        }

        if(Reflect.hasField(o,"result"))
        {
            var token = pendingTokens.get(o.id);
            pendingTokens.remove(o.id);
            token.applyResult(o.result);
            pendingMessage = false;
            doNext();

        }
    }

    function call(method:String, ?params:Dynamic = null)
    {
        var request:Dynamic = {
            id: messageId++,
            method: method
        };
        if(params != null) request.params = params;

        if(!pendingMessage) return send(request);
        else requestQueue.push(request);
        return null;
    }

    function send(request:Dynamic):Token
    {
        var t = new Token();
        pendingMessage = true;
        pendingTokens.set(request.id, t);

        var errorHandler = function(error){
            if(error != null) t.applyError(error);
        }

        var msg = haxe.Json.stringify(request);
        trace("sending ... "+msg);
        socket.send( msg , errorHandler );
        return t;
    }

    function doNext()
    {
        if(requestQueue.length == 0) return;

        var f = function(){
            send( requestQueue.shift() );
        };
        untyped __js__ ('setTimeout(f,1000)');
    }
}
