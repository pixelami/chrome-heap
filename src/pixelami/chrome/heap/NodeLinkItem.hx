package pixelami.chrome.heap;

import pixelami.tree.TreeItem;

typedef NodeLinkItem = NodeLinkTreeItem<NodeLink>;

class NodeLinkTreeItem<T> extends TreeItem<T>
{
    public var depth:Int;

    override public function getDepth():Int
    {
        return depth;
    }
}