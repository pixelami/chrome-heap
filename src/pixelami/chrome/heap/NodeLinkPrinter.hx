package pixelami.chrome.heap;

import pixelami.chrome.Heap;
import pixelami.tree.TreeItemVisitor;
import pixelami.tree.TreeItem;

class NodeLinkPrinter implements TreeItemVisitor<TreeItem<NodeLink>>
{
    public var nodeLinkFormatFunc:NodeLink->String;

    var visitedItems:IntHash<TreeItem<NodeLink>>;
    var indent:String;


    public function new(?indentSize:Int = 3)
    {
        this.indent = "";
        for(i in 0...indentSize) indent += " ";

        visitedItems = new IntHash<TreeItem<NodeLink>>();
    }

    public function visit(item:TreeItem<NodeLink>)
    {
        print(item);

        var edge:Edge = item.item.edge;
        var index = -1;
        if(edge != null) index = edge.index;

        if(visitedItems.exists(index)) return;
        visitedItems.set(index, item);

        for(child in item.children)
        {
            child.accept(this);
        }
    }

    function print(item:TreeItem<NodeLink>)
    {
        var nodeLink = item.item;
        var itemDepth = item.getDepth();
        var sbuf = new StringBuf();

        if(itemDepth > 0)
        {
            var p:TreeItem<NodeLink> = item.parent;
            var symbols:Array<String> = [];

            var d = itemDepth;
            while(d > 0)
            {
                symbols.push(p.isLastChild() ? " " : util.ConsoleStyle.format("│", util.ConsoleStyle.whiteF));
                d--;
                p = p.parent;
            }
            symbols.reverse();
            var node:String = item.isLastChild() ? "└─" : "├─";
            symbols.push(util.ConsoleStyle.format(node, util.ConsoleStyle.whiteF));
            sbuf.add( symbols.join(indent) );
        }

        sbuf.add(formatNodeLink(nodeLink));
        trace(sbuf.toString());
    }

    function formatNodeLink(link:NodeLink)
    {
        return nodeLinkFormatFunc(link);
    }
}