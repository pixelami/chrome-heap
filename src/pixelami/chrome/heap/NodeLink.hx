package pixelami.chrome.heap;

import pixelami.chrome.Heap;

class NodeLink
{
    public var name(default, null):String;
    public var node(default, null):Node;
    public var edge(default, null):Edge;

    public function new(name:String, node:Node, edge:Edge)
    {
        this.name = name;
        this.node = node;
        this.edge = edge;
    }
}
