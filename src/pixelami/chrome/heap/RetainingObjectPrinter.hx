package pixelami.chrome.heap;

class RetainingObjectPrinter extends NodeLinkPrinter
{
    public var rootDepth:Int;
    override function formatNodeLink(link:NodeLink)
    {
        if(link.node.depth < rootDepth) return NodeLinkFormatter.formatInterestingRetainingLink(link);
        return nodeLinkFormatFunc(link);
    }
}
