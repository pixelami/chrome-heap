package pixelami.chrome.heap;

import pixelami.chrome.Heap;

class RetainingObjectTree extends ObjectTree
{
    override function addChild(linkItem:NodeLinkItem, ?parent:NodeLinkItem = null)
    {
        if(parent != null) parent.addChild(linkItem);
    }

    override function getEdges(node:Node):Array<Edge>
    {
        return heap.findEdgesToNode(node);
    }

    override function getLink(edge:Edge):NodeLink
    {
        var prop = null;
        if(edge.type == "property") prop = edge.name;
        if(prop != null)
        {
            var node = heap.getNodeFromEdge(edge);
            var link = new NodeLink(prop, node, edge);
            return link;
        }
        return null;
    }
}
