package pixelami.chrome.heap;

import pixelami.chrome.Heap;

class ObjectTree
{
    public var rootNode(default, null):NodeLinkItem;

    var visitedEdge:IntHash<NodeLinkItem>;
    var heap:Heap;

    var visitedNodes:IntHash<Node>;
    var propertiesToIgnore:Array<String>;

    public function new(heap:Heap, node:Node, ?ignore:Array<String> = null)
    {
        propertiesToIgnore = ignore == null ? [] : ignore;
        visitedNodes = new IntHash<Node>();
        visitedEdge = new IntHash<NodeLinkItem>();
        this.heap = heap;
        rootNode = new NodeLinkItem(new NodeLink(".",node, null));
        rootNode.depth = 0;
        walkChildren(node, rootNode, 1);
    }

    function walkChildren(node:Node, parent:NodeLinkItem, depth)
    {
        if(visitedNodes.exists(node.id)) return;
        visitedNodes.set(node.id, node);

        var edges = getEdges(node);
        var links = [];
        for(edge in edges)
        {
            if(!visitedEdge.exists(edge.index))
            {
                var link = getLink(edge);
                if(link != null) links.push(link);
            }
        }

        for(l in links)
        {
            var linkItem = new NodeLinkItem(l);
            linkItem.depth = depth;
            visitedEdge.set(l.edge.index, linkItem);
            addChild(linkItem, parent);
            depth ++;
            walkChildren(l.node, linkItem, depth);
            depth --;
        }
    }

    function addChild(linkItem:NodeLinkItem, ?parent:NodeLinkItem = null)
    {
        if(parent == null) return;
        // don't follow anything specified in propertiesToIgnore
        if(Lambda.indexOf(propertiesToIgnore, parent.item.name) > -1) return;
        parent.addChild(linkItem);
    }

    function getEdges(node:Node):Array<Edge>
    {
        return node.edges;
    }

    function getLink(edge:Edge):NodeLink
    {
        var prop = null;
        if(edge.type == "property") prop = edge.name;
        if(prop != null)
        {
            var node = heap.getNodeAt(edge.to_node);
            var link = new NodeLink(prop, node, edge);
            return link;
        }
        return null;
    }

    public function hasNodeId(id:Int):Bool
    {
        return visitedNodes.exists(id);
    }
}

