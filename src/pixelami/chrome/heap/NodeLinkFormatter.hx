package pixelami.chrome.heap;

class NodeLinkFormatter
{
    public static function formatPropertyLink(link:NodeLink):String
    {
        var name = link.node.type == "closure" ? "function" : link.node.name;
        var sb:StringBuf = new StringBuf();
        sb.add(util.ConsoleStyle.format(link.name, util.ConsoleStyle.cyanF));
        sb.add(" -> ");
        sb.add(util.ConsoleStyle.format(name, util.ConsoleStyle.purpleF));
        sb.add(" @" + link.node.index);
        sb.add(" " + link.node.depth);
        return sb.toString();
    }

    public static function formatRetainingLink(link:NodeLink):String
    {
        var name = link.node.type == "closure" ? "function" : link.node.name;
        var sb:StringBuf = new StringBuf();
        sb.add(util.ConsoleStyle.format(link.name, util.ConsoleStyle.cyanF));
        sb.add(" in ");
        sb.add(util.ConsoleStyle.format(name, util.ConsoleStyle.yellowF));
        sb.add(" @" + link.node.index);
        sb.add(" " + link.node.depth);
        return sb.toString();
    }

    public static function formatInterestingRetainingLink(link:NodeLink):String
    {
        var name = link.node.type == "closure" ? "function" : link.node.name;
        var sb:StringBuf = new StringBuf();
        sb.add(util.ConsoleStyle.format(link.name, util.ConsoleStyle.cyanF));
        sb.add(" in ");
        sb.add(util.ConsoleStyle.format(name, util.ConsoleStyle.redF));
        sb.add(" @" + link.node.index);
        sb.add(" " + link.node.depth);
        return sb.toString();
    }

}
