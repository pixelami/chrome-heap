package pixelami.chrome;

import Lambda;

class Heap
{
    var source:String;
    var obj:Dynamic;

    var nodeIndex:IntHash<Dynamic>;
    var nodeId:IntHash<Dynamic>;
    //Maps a Node id to all edges that specify that Node id in their 'to_node' field
    var toNodeEdges:IntHash<Array<Edge>>;

    // Edges that lead from a node
    var edgeFromNode:IntHash<Int>;

    // Maps edge index to 'owner' node ids
    //var edgeToNodeMap:IntHash<Int>;

    public var snapshot(get_snapshot, null):Snapshot;
    function get_snapshot():Snapshot
    {
        return obj.snapshot;
    }

    public var nodes(get_nodes, null):Array<Int>;
    function get_nodes():Array<Int>
    {
        return obj.nodes;
    }

    public var edges(get_edges, null):Array<Int>;
    function get_edges():Array<Int>
    {
        return obj.edges;
    }

    public var strings(get_strings, null):Array<String>;
    function get_strings():Array<String>
    {
        return obj.strings;
    }

    public function new(source:String)
    {
        this.source = source;
        nodeIndex = new IntHash<Dynamic>();
        nodeId = new IntHash<Dynamic>();
        toNodeEdges = new IntHash<Array<Edge>>();
        edgeFromNode = new IntHash<Int>();
        //edgeToNodeMap = new IntHash<Int>();
        obj = haxe.Json.parse(source);
        buildGraph();

        computeDepths(nodeIndex.get(0), 0);
    }

    function buildGraph()
    {
        var edgeStart = 0;
        var edgeEnd = 0;
        for(i in 0...snapshot.node_count)
        {
            var node = buildNodeAt(i);
            edgeEnd = edgeStart + node.edge_count;
            for(ii in edgeStart...edgeEnd)
            {
                var edge = buildEdgeAt(ii, node.index);
                //edgeToNodeMap.set(edge.index, node.index);
                node.edges.push(edge);
            }
            edgeStart = edgeEnd;
            nodeIndex.set(node.index, node);

        }
    }


    function buildNodeAt(index:Int):Node
    {
        var fieldsLength = snapshot.meta.node_fields.length;
        var start = index * fieldsLength;
        var codes:Array<Int> = nodes.slice(start, start + fieldsLength);

        var node = {
            index: start,
            type: snapshot.meta.node_types[0][codes[0]],
            name: strings[codes[1]],
            name_index: codes[1],
            id: codes[2],
            self_size: codes[3],
            edge_count: codes[4],
            edges: [],
            depth:null,
            parents: null
        };
        nodeId.set(node.id, node);
        return node;
    }

    function buildEdgeAt(index:Int, nodeIndex:Int):Edge
    {
        var fieldsLength = snapshot.meta.edge_fields.length;
        var start = index * fieldsLength;
        var codes:Array<Int> = edges.slice(start, start + fieldsLength);

        var edgeType = snapshot.meta.edge_types[0][codes[0]];
        var name:String = null;
        if(!(edgeType == 'element' || edgeType == 'hidden'))
        {
            name = strings[codes[1]];
        }
        var edge = {
            index: index,
            type: edgeType,
            name: name,
            name_or_index: codes[1],
            from_node: nodeIndex,
            to_node: codes[2]
        };
        if(!toNodeEdges.exists(edge.to_node)) toNodeEdges.set(edge.to_node, []);
        toNodeEdges.get(edge.to_node).push(edge);

        edgeFromNode.set(edge.index, nodeIndex);
        return edge;
    }

    public function getNodeAt(index:Int)
    {
        return nodeIndex.get(index);
    }

    public function getNodeById(id:Int)
    {
        trace(id);
        return nodeId.get(id);
    }

    public function getNodeFromEdge(edge:Edge):Node
    {
        var nodeId = edgeFromNode.get(edge.index);
        return nodeIndex.get(nodeId);
    }

    public function findNodesByName(name:String, ?typesFilter:Array<String>):Array<Node>
    {
        var _nodes:Array<Node> = [];
        for(i in 0...snapshot.node_count)
        {
            var nIndex = i * snapshot.meta.node_fields.length;
            var nodeName = strings[nodes[nIndex + 1]];
            if(nodeName.indexOf(name) > -1)
            {
                var n = nodeIndex.get(nIndex);
                if(typesFilter != null)
                {
                    if(Lambda.indexOf(typesFilter, n.type) > -1) _nodes.push(n);
                }
                else _nodes.push(n);
            }
        }
        return _nodes;
    }

    public function findEdgesToNode(node:Node):Array<Edge>
    {
        return toNodeEdges.get(node.index);
    }

    public function findEdgesToNodeId(id:Int):Array<Edge>
    {
        return toNodeEdges.get(id);
    }

    function computeDepths(node:Node, depth)
    {
        var heap = this;
        //trace(node);
        if(node.depth == null)
        {
            node.depth = depth;
            node.parents = [];
        }

        for(e in node.edges)
        {
            var child:Node = getNodeAt(e.to_node);

            if (child.depth != null && child.depth <= depth + 1)
            {
                //child.parents.push(node.index);
                //trace("ending" + child.depth);
                continue ;
            }

            //child.parents = [ node.index ];
            child.depth = depth + 1;

            computeDepths(child, depth + 1);
        }
    }
}

typedef Node = {
    name_index:Int,
    index:Int,
    type:Dynamic,
    name:String,
    id:Int,
    self_size:Int,
    edge_count:Int,
    edges:Array<Edge>,
    depth:Int,
    parents:Array<Int>
}

typedef Edge = {
    index:Int,
    type:Dynamic,
    name:String,
    name_or_index:Int,
    from_node:Int,
    to_node:Int
}

typedef Snapshot = {
    title:String,
    uid:String,
    meta:Meta,
    node_count:Int,
    edge_count:Int
}

typedef Meta = {
    node_fields:Array<String>,
    node_types:Array<Dynamic>,
    edge_fields:Array<String>,
    edge_types:Array<Dynamic>
}


