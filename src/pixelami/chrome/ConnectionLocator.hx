package pixelami.chrome;

import js.Node;
import pixelami.net.Token;

typedef PageInfo = {
    devtoolsFrontendUrl:String,
    faviconUrl: String,
    thumbnailUrl: String,
    title: String,
    url: String,
    webSocketDebuggerUrl: String
}

class ConnectionLocator
{
    var webSocketDebuggerUrl:String;
    var pageUrl:String;
    var token:Token;
    var tabLocatorUrl:String;

    public function new(tabLocatorUrl:String)
    {
        this.tabLocatorUrl = tabLocatorUrl;
    }

    public function getSocketFor(pageUrl:String):Token
    {
        token = new Token();
        this.pageUrl = pageUrl;

        var http = Node.require("http");
        var options = { hostname: "localhost", port: "9222", path: "/json" };

        http.get(options, function(response)
        {
            response.setEncoding('utf8');
            //trace("status: "+response.statusCode);
            response.on('data', onPageData);
        }).on('error', onError);

        return token;
    }

    function onPageData(data:String)
    {
        trace(data);

        var pages:Array<PageInfo> = haxe.Json.parse(data);
        var found:Bool = false;
        for(pageInfo in pages)
        {
            if(pageInfo.url == pageUrl)
            {
                webSocketDebuggerUrl = pageInfo.webSocketDebuggerUrl;
                found = true;
                break;
            }
        }

        if(found)
        {
            token.applyResult(webSocketDebuggerUrl);
        }
        else
        {
            token.applyError("page not found: "+pageUrl);
        }

    }

    function onError(error:Dynamic)
    {
        token.applyError(error);
    }
}
