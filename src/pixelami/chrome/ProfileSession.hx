package pixelami.chrome;

import pixelami.net.Token;

class ProfileSession
{
    var profiler:ChromeProfiler;
    var locator:ConnectionLocator;
    var connectionsUrl:String;
    // callback to tell us when a snapshot has been taken.
    public var onSnapshot:Heap -> Void;

    public function new(connectionsUrl:String = "http://locahost:9222/json")
    {
        this.connectionsUrl = connectionsUrl;
    }

    public function connect(targetUrl:String)
    {
        if(locator == null)
        {
            locator = new ConnectionLocator(connectionsUrl);
        }
        var t:Token = locator.getSocketFor(targetUrl);
        t.addHandlers(onConnectionResult, onConnectionError);
    }

    function onConnectionResult(result:String)
    {
        trace(result);
        profiler = new ChromeProfiler();
        profiler.onSnapshot = onSnapshot;
        profiler.connect(result);
    }

    function onConnectionError(error:String)
    {
        trace(error);
    }


}


