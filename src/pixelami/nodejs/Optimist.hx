package pixelami.nodejs;

@:extern('optimist')
extern class Optimist implements Dynamic
{
    public var argv:Dynamic;
    public function usage(msg:String):Void;
    public function demand(config:Dynamic):Void;
    @:extern('default')
    public function default_(config:Dynamic):Void;
}
