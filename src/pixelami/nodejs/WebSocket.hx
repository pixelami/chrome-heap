package pixelami.nodejs;

/*
Extern for Node "ws" lib
*/

extern class WebSocket
{
    @:overload(function(event:String, handler:Void->Void):Void{})
    public function on(event:String, handler:Dynamic->Void):Void;
    public function send(msg:String, ?errHandler:Dynamic -> Void = null):Void;
}
