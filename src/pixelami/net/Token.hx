package pixelami.net;
class Token
{
    var result:Dynamic -> Void;
    var error:Dynamic -> Void;

    public function new()
    {
    }

    public function addHandlers(result:Dynamic -> Void, error:Dynamic -> Void)
    {
        this.result = result;
        this.error = error;
    }

    public function applyResult(result:Dynamic)
    {
        if(this.result != null) this.result(result);
        destroy();
    }

    public function applyError(error:Dynamic)
    {
        if(this.error != null) this.error(error);
        destroy();
    }

    function destroy()
    {
        this.result = null;
        this.error = null;
    }
}
