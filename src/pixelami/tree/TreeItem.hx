package pixelami.tree;

class TreeItem<T>
{
    public var parent:TreeItem<T>;
    public var children(default, null):Array<TreeItem<T>>;
    public var item(default, null):T;


    public function new(item:T)
    {
        this.parent = null;
        this.item = item;
        this.children = [];
    }

    public function addChild(childItem:TreeItem<T>)
    {
        children.push(childItem);
        childItem.parent = this;
    }

    public function accept(visitor:TreeItemVisitor<TreeItem<T>>)
    {
        visitor.visit(this);
    }

    public function getDepth():Int
    {
        var d = 0;
        var p = parent;
        while(p != null)
        {
            p = p.parent;
            trace(p);
            d++;
        }
        return d;
    }

    public function isLastChild():Bool
    {
        if(parent == null) return true;
        return getIndex() == parent.children.length - 1;
    }

    public function getIndex():Int
    {
        if(parent == null) return 0;
        return Lambda.indexOf(parent.children, this);
    }
}
