package pixelami.tree;

interface TreeItemVisitor<TreeItem>
{
    function visit(item:TreeItem):Void;
}
