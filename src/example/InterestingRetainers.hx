package example;

import pixelami.nodejs.Optimist;
import pixelami.chrome.ProfileSession;
import pixelami.chrome.heap.RetainingObjectPrinter;
import pixelami.chrome.heap.ObjectTree;
import pixelami.chrome.heap.NodeLinkPrinter;
import pixelami.chrome.heap.NodeLinkFormatter;
import pixelami.chrome.heap.RetainingObjectTree;
import pixelami.chrome.Heap;

class InterestingRetainers
{
    public static function main():Void
    {
        var app = new InterestingRetainers();
    }

    var heap:Heap;
    var results:Array<Node>;
    var session:ProfileSession;
    var replSession:Dynamic;

    public function new()
    {
        configureOptions();

        var repl = js.Node.require("repl");
        replSession = repl.start(">");
        replSession.context.api = {o: printObject, find:findNodes, r:printRetainingObjects};
        replSession.context.session = session;

        replSession.on('exit', function(){
            trace("Goodbye");
        });
    }

    function configureOptions()
    {
        var optimist:Optimist = js.Node.require('optimist');
        optimist.usage("Usage: $0 -heap [heapfile] -debuggerUrl [url]");

        var argv = optimist.argv;

        if(argv.heap != null)
        {
            var contents = nsys.io.File.getContent(argv.heap);
            heap = new Heap(contents);
        }

        session = new ProfileSession();
        session.onSnapshot = onSnapshot;
    }

    function onSnapshot(heap:Heap):Void
    {
        this.heap = heap;
        replSession.context.heap = this.heap;
    }

    function find(name:String) { return findNodes(name); }
    function findNodes(name:String)
    {
        var filter = ["object"];
        results = heap.findNodesByName(name, filter);

        var r = [];
        for(result in results)
        {
            var s = result.name + " @" + result.id + " - " +result.depth;
            r.push(s);
        }
        return r;
    }

    function o(id:Int) { printObject(id); }
    function printObject(id:Int)
    {
        var nd = heap.getNodeById(id);
        var oTree = new ObjectTree(heap, nd, ["parent"]);

        var printer:NodeLinkPrinter = new NodeLinkPrinter();
        printer.nodeLinkFormatFunc = NodeLinkFormatter.formatPropertyLink;
        printer.visit(oTree.rootNode);
    }

    function r(id:Int) { printRetainingObjects(id); }
    function printRetainingObjects(id:Int)
    {
        var nd = heap.getNodeById(id);
        var oTree = new RetainingObjectTree(heap, nd);

        var printer = new RetainingObjectPrinter();
        printer.rootDepth = nd.depth;
        printer.nodeLinkFormatFunc = NodeLinkFormatter.formatRetainingLink;
        printer.visit(oTree.rootNode);

    }
}
